FROM adoptopenjdk/openjdk13-openj9:alpine-slim
EXPOSE 8081
ARG JAR_FILE=build/libs/stock-exchange-gateway-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]