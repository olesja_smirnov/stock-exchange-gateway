# stock-exchange-gateway
You need to run `stock-exchange-webapp` application as frontend module or to run both modules via Docker (check configuration below)

# Getting Started
Set SDK for project (project compiled with 13)

## IDEA configuration
Install plugin "Lombok" to your editor (for older IDEA version)

## API documentation
* Open API doc via Swagger http://localhost:8081/swagger-ui/

## DB & APPLICATION DOCKER CONFIGURATION
build & run docker container:
`docker-compose up`

## DB CONFIGURATION (without docker)
DriverClassName: jdbc:postgresql://localhost:5432/postgres
   
## Docker configuration (if needed to run application without Postgres image)
* For creating an image run: `docker build -t stock-exchange-gw .`
* Then run the container from the image: `docker run -it -p 8081:8081 -d stock-exchange-gw`

## Task
**Stock Data**

Address https://nasdaqbaltic.com/statistics/en/shares?download=1&date=2021-08-03 contains stock information for given day.

Your job is to create a system that retrieves stock info for the last 10 days from that site and keeps itself updated.
Meaning your system should do the following:
* Download last 10 days information from the www.nasdaqbaltic.com website after every 30 minutes.
If information already exists for previous days, you should skip them.
* Current day stock information is thus updated every 30 minutes.

**User interface should contain following functionality:**
* Show list of all the days that the system has information for;
* If clicked on the day then similar view should be displayed like nasdaqomxbaltic.com daily view;
* Create a view, where you can select the time period and top 10 most actively traded stocks for that time period are shown (stocks with biggest volume); 
* Implement user interface as a single-page application using modern framework (eg. Angular2, React, Vue); 
* Communication with the backend should be done through REST services

**Bonus:**
* Create a Docker container (or a set of containers) that runs the application;
* Controllers and services (if there are any) should be unit tested (using for example Jasmine).

**Technical requirements:**
* Application has to be covered with unit tests;
* Use Java 11 or later;
* Servlet container: use Spring Boot with embedded container or standalone Apache Tomcat;
* Database should be one of the following:
** PostgreSQL
** MSSQL 
** HSQLDB 
* Use Gradle to build the application; 
* For external libraries use also Gradle; 
* There should be a script for creating the database in file /sql/database.sql (except if running on container) 
* Add installation guide, which when followed in a new environment should lead to a working application