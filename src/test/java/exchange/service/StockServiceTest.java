package exchange.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import exchange.domain.model.StockData;
import exchange.domain.repo.StockDataRepository;
import exchange.mapper.StockListResponseMapper;
import exchange.web.request.TopTradesRequest;
import exchange.web.response.DateResponse;
import exchange.web.response.GrandTotalTradesResponse;
import exchange.web.response.StockListResponse;
import exchange.web.response.TotalMarketResponse;

@ExtendWith(MockitoExtension.class)
public class StockServiceTest {

    private static final LocalDate REQUEST_DATE = LocalDate.of(2021, 10, 30);
    private static final LocalDate DATE_1 = LocalDate.of(2021, 10, 30);
    private static final LocalDate FROM = LocalDate.of(2021, 5, 10);
    private static final LocalDate TILL = LocalDate.of(2021, 12, 24);
    private static final String ISIN = "LT0000128696";
    private static final String DATE_STRING = "2021-10-30";
    private static final Long ID = 10L;
    private static final Double TRADES = 260.123d;
    private static final Double ROUNDED_TRADES = 260.12;

    private static final StockData STOCK_DATA = StockData.builder()
            .stockDate(DATE_1)
            .trades(TRADES)
            .build();

    @InjectMocks
    private StockService stockService;

    @Mock
    private StockDataRepository stockDataRepository;

    @Mock
    private StockListResponseMapper stockListResponseMapper;

    @Test
    void testNewStockDataCreation() {
        stockService.createNewStockData(List.of(STOCK_DATA));

        verify(stockDataRepository, times(1)).saveAll(List.of(STOCK_DATA));
    }

    @Test
    void shouldCheckIfStockDataExists() {
        when(stockDataRepository.findByStockDate(any())).thenReturn(Collections.emptyList());

        boolean result = stockService.checkIfDataExists(REQUEST_DATE);

        assertFalse(result);
    }

    @Test
    void shouldUpdateExistingStock() {
        StockData existingStockData = StockData.builder()
                .id(ID)
                .ISIN(ISIN)
                .build();

        StockData newStockData = StockData.builder()
                .id(ID)
                .ISIN(ISIN)
                .build();

        when(stockDataRepository.findByStockDate(any())).thenReturn(List.of(existingStockData));

        stockService.processCurrentStockData(List.of(newStockData), REQUEST_DATE);

        verify(stockDataRepository, times(1)).saveAll(List.of(newStockData));
    }

    @Test
    void shouldReturnExistingStockDates() {
        DateResponse response = DateResponse.builder()
                .date(DATE_1)
                .build();

        when(stockDataRepository.findAll()).thenReturn(List.of(STOCK_DATA));

        List<DateResponse> result = stockService.getAllExistingDates();

        assertEquals(result.get(0), response);
    }

    @Test
    void shouldReturnTopActiveTrades() {
        TopTradesRequest request = TopTradesRequest.builder()
                .from(FROM)
                .till(TILL)
                .build();

        StockListResponse response = StockListResponse.builder()
                .stockDate(DATE_1)
                .build();

        when(stockDataRepository.findByStockDateBetweenOrderByVolumeDesc(any(), any())).thenReturn(List.of(STOCK_DATA));
        when(stockListResponseMapper.convertTo(any())).thenReturn(response);

        List<StockListResponse> result = stockService.getTopActiveTrades(request);

        assertFalse(result.isEmpty());
        assertEquals(result.get(0).getStockDate(), DATE_1);
    }

    @Test
    void shouldReturnGrandTotalTrades() {
        GrandTotalTradesResponse balticRegulatedMarket = GrandTotalTradesResponse.builder()
                .totalTrades(ROUNDED_TRADES)
                .build();

        when(stockDataRepository.findByStockDateAndSegmentIn(any(), any())).thenReturn(List.of(STOCK_DATA));

        TotalMarketResponse result = stockService.getGrandTotalTrades(DATE_STRING);

        assertNotNull(result);
        assertEquals(result.getBalticRegulatedMarket().getTotalTrades(), balticRegulatedMarket.getTotalTrades());
    }

    @Test
    void testDoubleRounding() {
        Double result = stockService.getTotalTrades(List.of(STOCK_DATA));

        assertEquals(result, 260.12);
    }

}