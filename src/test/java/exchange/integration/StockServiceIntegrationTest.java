package exchange.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URL;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import exchange.web.request.DailyRequest;
import exchange.web.request.TopTradesRequest;
import exchange.web.response.DailyResponse;
import exchange.web.response.DateResponse;
import exchange.web.response.StockListResponse;
import exchange.web.response.TotalMarketResponse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StockServiceIntegrationTest {

    private static final String API = "/api/stock";
    private static final String BASE_URL = "http://localhost:8081";
    private static final LocalDate FROM = LocalDate.now();
    private static final String REQUEST_DATE = LocalDate.now().toString();
    private static final LocalDate TILL = LocalDate.now();

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void testSuccessForReceivingStockDates() throws Exception {
        ResponseEntity<DateResponse[]> response = restTemplate.getForEntity(new URL(BASE_URL + API).toString(), DateResponse[].class);

        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    void testSuccessDailyStock() throws Exception {
        DailyRequest request = DailyRequest.builder()
                .segment("BALTIC_MAIN")
                .build();

        ResponseEntity<DailyResponse> response = restTemplate.postForEntity(new URL( BASE_URL + API+ "/" + REQUEST_DATE).toString(), request, DailyResponse.class);

        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    void testSuccessForReceivingTopTrades() throws Exception {
        TopTradesRequest request = TopTradesRequest.builder()
                .from(FROM)
                .till(TILL)
                .build();

        ResponseEntity<StockListResponse[]> response = restTemplate.postForEntity(new URL(BASE_URL + API).toString(), request, StockListResponse[].class);

        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    void testSuccessGrandTotalTrades() throws Exception {
        ResponseEntity<TotalMarketResponse> response = restTemplate.getForEntity(new URL(BASE_URL + API+ "/" + REQUEST_DATE).toString(), TotalMarketResponse.class);

        assertEquals(200, response.getStatusCodeValue());
    }

}