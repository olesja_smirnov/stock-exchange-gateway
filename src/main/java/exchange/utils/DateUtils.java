package exchange.utils;

import java.time.LocalDate;

public final class DateUtils {

    public static LocalDate currentLocalDate() { return LocalDate.now(); }

}