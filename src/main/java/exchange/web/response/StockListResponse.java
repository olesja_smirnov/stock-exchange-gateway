package exchange.web.response;

import java.math.BigDecimal;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.sun.istack.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StockListResponse {

    @NotNull
    private Long id;

    @NotNull
    private LocalDate stockDate;

    @NotNull
    private String ticker;

    @NotNull
    private String companyName;

    @NotNull
    private String segment;

    private BigDecimal lastPrice;

    private Double priceChange;

    private BigDecimal bestBid;

    private BigDecimal bestAsk;

    @NotNull
    private Double trades;

    @NotNull
    private Double volume;

    @NotNull
    private Double turnover;

}