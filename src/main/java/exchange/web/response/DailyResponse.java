package exchange.web.response;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.sun.istack.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DailyResponse {

    private List<StockListResponse> stocks;

    @NotNull
    private Double totalTrades;

    @NotNull
    private Double totalVolume;

    @NotNull
    private Double totalTurnover;

}