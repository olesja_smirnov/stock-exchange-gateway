package exchange.web.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TotalMarketResponse {

    private GrandTotalTradesResponse balticRegulatedMarket;

    private GrandTotalTradesResponse allMarkets;

}