package exchange.web.response;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.sun.istack.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DateResponse {

    @NotNull
    private LocalDate date;

}