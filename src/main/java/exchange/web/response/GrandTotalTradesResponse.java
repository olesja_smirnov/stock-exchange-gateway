package exchange.web.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GrandTotalTradesResponse {

    private Double totalTrades;

    private Double totalVolume;

    private Double totalTurnover;

}