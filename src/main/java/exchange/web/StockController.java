package exchange.web;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import exchange.service.StockService;
import exchange.web.request.DailyRequest;
import exchange.web.request.TopTradesRequest;
import exchange.web.response.DailyResponse;
import exchange.web.response.DateResponse;
import exchange.web.response.StockListResponse;
import exchange.web.response.TotalMarketResponse;

@RestController
@RequestMapping(path = "/api/stock")
@Api(tags = "stock", value = "/api/stock")
public class StockController {

    @Autowired
    private StockService service;

    @ApiOperation("Get all Stock dates")
    @GetMapping
    public List<DateResponse> getAllExistingDates() {

        return service.getAllExistingDates();
    }

    @ApiOperation("Get Daily Stock exchange")
    @PostMapping("/{date}")
    public DailyResponse getDailyStock(@PathVariable String date, @RequestBody DailyRequest req) {

        return service.getDailyStocks(date, req);
    }

    @ApiOperation("Get 10 Top active trades")
    @PostMapping
    public List<StockListResponse> getTopTrades(@RequestBody TopTradesRequest request) {

        return service.getTopActiveTrades(request);
    }

    @ApiOperation("Get Grand Total Daily trades")
    @GetMapping("/{date}")
    public TotalMarketResponse getGrandTotalDailyTrades(@PathVariable String date) {

        return service.getGrandTotalTrades(date);
    }

}