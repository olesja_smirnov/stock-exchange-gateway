//package exchange.mapper;
//
//import java.util.List;
//
//import org.dozer.DozerConverter;
//import org.springframework.stereotype.Service;
//
//import exchange.domain.model.StockData;
//import exchange.web.response.DailyResponse;
//
//@Service
//public class DailyResponseMapper extends DozerConverter<List<StockData>, DailyResponse> {
//
//    public DailyResponseMapper() {
//        super(StockData.class, DailyResponse.class);
//    }
//
//
//    @Override
//    public DailyResponse convertTo(List<StockData> source, DailyResponse destination) {
//        return null;
//    }
//
//    @Override
//    public List<StockData> convertFrom(DailyResponse source, List<StockData> destination) {
//        return null;
//    }
//
//}