package exchange.mapper;

import org.dozer.DozerConverter;
import org.springframework.stereotype.Service;

import exchange.domain.model.StockData;
import exchange.web.response.StockListResponse;

@Service
public class StockListResponseMapper extends DozerConverter<StockData, StockListResponse> {

    public StockListResponseMapper() {
        super(StockData.class, StockListResponse.class);
    }

    @Override
    public StockListResponse convertTo(StockData source, StockListResponse destination) {

        return StockListResponse.builder()
                .id(source.getId())
                .stockDate(source.getStockDate())
                .companyName(source.getCompanyName())
                .ticker(source.getTicker())
                .segment(source.getSegment().name())
                .lastPrice(source.getLastPrice())
                .priceChange(source.getPriceChange())
                .bestBid(source.getBestBid())
                .bestAsk(source.getBestAsk())
                .trades(source.getTrades())
                .volume(source.getVolume())
                .turnover(source.getTurnover())
                .build();
    }

    @Override
    public StockData convertFrom(StockListResponse source, StockData destination) {
        return null;
    }

}