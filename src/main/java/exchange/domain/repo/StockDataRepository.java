package exchange.domain.repo;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import exchange.domain.enums.Segment;
import exchange.domain.model.StockData;

@Repository
public interface StockDataRepository extends JpaRepository<StockData, Long> {

    List<StockData> findByStockDate(LocalDate requestDate);

    List<StockData> findByStockDateAndSegmentOrderByCompanyName(LocalDate currentLocalDate, Segment segment);

    List<StockData> findByStockDateBetweenOrderByVolumeDesc(LocalDate from, LocalDate till);

    List<StockData> findByStockDateAndSegmentIn(LocalDate requestedDate, List<Segment> segments);

}