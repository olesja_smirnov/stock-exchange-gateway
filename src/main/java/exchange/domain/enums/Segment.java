package exchange.domain.enums;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public enum Segment {

    BALTIC_MAIN,
    BALTIC_SECONDARY,
    FIRST_NORTH_BALTIC,

    @JsonEnumDefaultValue
    UNKNOWN

}