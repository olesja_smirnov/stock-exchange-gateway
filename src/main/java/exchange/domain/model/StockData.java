package exchange.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.sun.istack.NotNull;

import exchange.domain.enums.Segment;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(uniqueConstraints = { @UniqueConstraint(name = "unique__stock_date__and__isin", columnNames = { "stockDate", "ISIN" }) })
public class StockData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    @NotNull
    private LocalDate stockDate;

    @Column
    @NotNull
    private String ticker;

    @Column
    @NotNull
    private String companyName;

    @Column
    @NotNull
    private String ISIN;

    @Column
    @NotNull
    private String currency;

    @Column
    @NotNull
    private String marketPlace;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Segment segment;

    @Column
    @Min(value = 0)
    private BigDecimal averagePrice;

    @Column
    @Min(value = 0)
    private BigDecimal openPrice;

    @Column
    @Min(value = 0)
    private BigDecimal highPrice;

    @Column
    @Min(value = 0)
    private BigDecimal lowPrice;

    @Column
    @Min(value = 0)
    private BigDecimal lastClose;

    @Column
    @Min(value = 0)
    private BigDecimal lastPrice;

    @Column
    private Double priceChange;

    @Column
    @Min(value = 0)
    private BigDecimal bestBid;

    @Column
    @Min(value = 0)
    private BigDecimal bestAsk;

    @Column
    @Min(value = 0)
    @NotNull
    @Builder.Default
    private Double trades = 0d;

    @Column
    @NotNull
    @Builder.Default
    @Min(value = 0)
    private Double volume = 0d;

    @Column
    @NotNull
    @Min(value = 0)
    @Builder.Default
    private Double turnover = 0d;

    @Column
    @NotNull
    private String industry;

    @Column
    @NotNull
    private String superSector;

}