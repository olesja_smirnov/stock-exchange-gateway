package exchange.service;

import static exchange.domain.enums.Segment.BALTIC_MAIN;
import static exchange.domain.enums.Segment.BALTIC_SECONDARY;
import static exchange.utils.DateUtils.currentLocalDate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import exchange.domain.enums.Segment;
import exchange.domain.model.StockData;
import exchange.domain.repo.StockDataRepository;
import exchange.mapper.StockListResponseMapper;
import exchange.web.request.DailyRequest;
import exchange.web.request.TopTradesRequest;
import exchange.web.response.DailyResponse;
import exchange.web.response.DateResponse;
import exchange.web.response.GrandTotalTradesResponse;
import exchange.web.response.StockListResponse;
import exchange.web.response.TotalMarketResponse;

@Service
@Slf4j
public class StockService {

    @Autowired
    private StockDataRepository stockRepository;

    @Autowired
    private StockListResponseMapper stockListResponseMapper;


    @Transactional
    public boolean checkIfDataExists(LocalDate requestDate) {
        List<StockData> stock = stockRepository.findByStockDate(requestDate);

        return !stock.isEmpty();
    }

    @Transactional
    public void createNewStockData(List<StockData> stocks) {
        stockRepository.saveAll(stocks);
    }

    @Transactional
    public void processCurrentStockData(List<StockData> stocks, LocalDate date) {
        List<StockData> existingStocks = stockRepository.findByStockDate(date);
        if (existingStocks.isEmpty()) {
            log.debug("No records for today, creating new...");
            createNewStockData(stocks);
            log.info("Today's {} Stock data is created.", currentLocalDate());
            return;
        }

        updateCurrentStock(existingStocks, stocks);
    }

    void updateCurrentStock(List<StockData> existingStocks, List<StockData> updatedStocks) {
        log.debug("Records for today already exist, updating them...");
        List<StockData> result = new ArrayList<>();
        for (StockData company : updatedStocks) {
            Optional<StockData> filteredData = existingStocks.stream()
                    .filter(row -> row.getISIN().equals(company.getISIN()))
                    .map(row -> getUpdatedCompany(row.getId(), company))
                    .findAny();

            filteredData.ifPresent(result::add);
        }
        stockRepository.saveAll(result);
        log.info("Existing today's {} Stock data is updated.", currentLocalDate());
    }

    private StockData getUpdatedCompany(Long id, StockData row) {
        row.setId(id);

        return row;
    }

    @Transactional
    public List<DateResponse> getAllExistingDates() {
        List<StockData> all = stockRepository.findAll();

        return all.stream()
                .map(s -> getDateResponseBuilder(s.getStockDate()))
                .distinct()
                .sorted(Comparator.comparing(DateResponse::getDate).reversed())
                .collect(Collectors.toList());
    }

    private DateResponse getDateResponseBuilder(LocalDate stockDate) {

        return DateResponse.builder()
                .date(stockDate)
                .build();
    }

    @Transactional
    public List<StockListResponse> getTopActiveTrades(TopTradesRequest request) {
        List<StockData> stockBetweenDates = stockRepository.findByStockDateBetweenOrderByVolumeDesc(request.getFrom(), request.getTill());

        if (stockBetweenDates.size() > 10) {
            List<StockData> result = stockBetweenDates.subList(0, 10);
            return getConvertedStocks(result);
        }

        return getConvertedStocks(stockBetweenDates);
    }

    @Transactional
    public DailyResponse getDailyStocks(String requestedDate, DailyRequest req) {
        LocalDate date = LocalDate.parse(requestedDate);
        Segment segment = Segment.valueOf(req.getSegment());
        List<StockData> stocks = stockRepository.findByStockDateAndSegmentOrderByCompanyName(date, segment);

        return getDailyResponseBuilder(stocks);
    }

    private DailyResponse getDailyResponseBuilder(List<StockData> stocks) {

        return DailyResponse.builder()
                .stocks(getConvertedStocks(stocks))
                .totalTrades(getTotalTrades(stocks))
                .totalVolume(getTotalVolume(stocks))
                .totalTurnover(getTotalTurnover(stocks))
                .build();
    }

    Double getTotalTrades(List<StockData> stocks) {
        double sum = stocks.stream()
                .mapToDouble(StockData::getTrades)
                .sum();

        return Math.round(sum * 100.0) / 100.0;
    }

    private Double getTotalVolume(List<StockData> stocks) {
        double sum = stocks.stream()
                .mapToDouble(StockData::getVolume)
                .sum();

        return Math.round(sum * 100.0) / 100.0;
    }

    private Double getTotalTurnover(List<StockData> stocks) {
        double sum = stocks.stream()
                .mapToDouble(StockData::getTurnover)
                .sum();

        return Math.round(sum * 100.0) / 100.0;
    }

    private List<StockListResponse> getConvertedStocks(List<StockData> stocks) {

        return stocks.stream()
                .map(stockListResponseMapper::convertTo)
                .collect(Collectors.toList());
    }

    @Transactional
    public TotalMarketResponse getGrandTotalTrades(String requestedDate) {
        LocalDate date = LocalDate.parse(requestedDate);

        return TotalMarketResponse.builder()
                .balticRegulatedMarket(getBalticRegulatedMarketGrandTotal(date))
                .allMarkets(getAllMarketGrandTotal(date))
                .build();
    }

    private GrandTotalTradesResponse getBalticRegulatedMarketGrandTotal(LocalDate date) {
        List<Segment> segments = new ArrayList<>();
        segments.add(BALTIC_MAIN);
        segments.add(BALTIC_SECONDARY);
        List<StockData> balticStocks = stockRepository.findByStockDateAndSegmentIn(date, segments);

        return getTotalMarketBuilder(balticStocks);
    }

    private GrandTotalTradesResponse getAllMarketGrandTotal(LocalDate date) {
        List<StockData> allStocks = stockRepository.findByStockDate(date);

        return getTotalMarketBuilder(allStocks);
    }

    private GrandTotalTradesResponse getTotalMarketBuilder(List<StockData> stocks) {

        return GrandTotalTradesResponse.builder()
                .totalTrades(getTotalTrades(stocks))
                .totalVolume(getTotalVolume(stocks))
                .totalTurnover(getTotalTurnover(stocks))
                .build();
    }

}