package exchange.service;

import static exchange.domain.enums.Segment.BALTIC_MAIN;
import static exchange.domain.enums.Segment.BALTIC_SECONDARY;
import static exchange.domain.enums.Segment.FIRST_NORTH_BALTIC;
import static exchange.domain.enums.Segment.UNKNOWN;
import static exchange.errors.ErrorMsg.ATTEMPT;
import static exchange.errors.ErrorMsg.ERROR;
import static exchange.errors.ErrorMsg.SUCCESS;
import static exchange.utils.DateUtils.currentLocalDate;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import exchange.domain.enums.Segment;
import exchange.domain.model.StockData;

@Service
@Slf4j
public class SchedulerService {

    private static final String FILE_NAME = "stock-exchange.xlsx";
    private static final String FILE_URL = "https://nasdaqbaltic.com/statistics/en/shares?download=1&date=";

    @Autowired
    private StockService stockService;

    @Scheduled(fixedDelayString = "${scheduler.fixed-delay}")
    public void runScheduler() throws IOException {
        log.info(ATTEMPT + "Starting scheduled job at {}", currentLocalDate());
        int maxDaysForRequest = 10;
        for (int i = maxDaysForRequest; i > 0; i--) {
            downloadOldStockData(i);
        }
        downloadCurrentDayStockData();
    }

    private void downloadOldStockData(int days) throws IOException {
        LocalDate requestDate = currentLocalDate().minusDays(days);
        boolean isExists = stockService.checkIfDataExists(requestDate);
        if (isExists) {
            log.info("Stock data already exists for {}", requestDate);
            return;
        }

        downloadStockFile(requestDate);

        new Thread(() -> {
            try {
                List<StockData> downloadedStockData = insertExcelToStockData(requestDate);
                stockService.createNewStockData(downloadedStockData);
            } catch (Exception e) {
                log.warn("Stock data migration failed", e);
                e.printStackTrace();
            }
        }).start();
    }

    private void downloadStockFile(LocalDate requestDate) throws IOException {
        String requestUrl = FILE_URL + requestDate;
        log.info(ATTEMPT + "Starting to download Stock data of {}", requestDate);

        try (BufferedInputStream in = new BufferedInputStream(new URL(requestUrl).openStream());
             FileOutputStream fileOutputStream = new FileOutputStream(FILE_NAME)) {

            byte[] dataBuffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }
        } catch (IOException e) {
            log.error(ERROR + "Failed to download stock data for {}.", requestDate);
            throw new IOException(e);
        }
        log.info(SUCCESS + "Stock data is downloaded for {}", requestDate);
    }

    private List<StockData> insertExcelToStockData(LocalDate requestDate) {
        log.info(ATTEMPT + "Starting to extract Stock data from downloaded file.");
        List<StockData> stocks = new ArrayList<>();
        try {
            FileInputStream file = new FileInputStream(FILE_NAME);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.rowIterator();

            while (rows.hasNext()) {
                Row row = rows.next();
                    if(row.getRowNum()==0){
                        continue; // skip the first row
                    }

                log.info("Processing row nr {} ...", row.getRowNum());

                StockData downloadedStockData = getStockBuilderFromExcel(row, requestDate);
                stocks.add(downloadedStockData);
            }
            file.close();
            log.info(SUCCESS + "Stock file is processed.");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return stocks;
    }

    private StockData getStockBuilderFromExcel(Row row, LocalDate requestDate) {
        try {
            return StockData.builder()
                    .stockDate(requestDate)
                    .ticker(getCellStringValue(row, 0))
                    .companyName(getCellStringValue(row, 1))
                    .ISIN(getCellStringValue(row, 2))
                    .currency(getCellStringValue(row, 3))
                    .marketPlace(getCellStringValue(row, 4))
                    .segment(getSegment(row))
                    .averagePrice(getCellBigDecimalValue(row, 6))
                    .openPrice(getCellBigDecimalValue(row, 7))
                    .highPrice(getCellBigDecimalValue(row, 8))
                    .lowPrice(getCellBigDecimalValue(row, 9))
                    .lastClose(getCellBigDecimalValue(row, 10))
                    .lastPrice(getCellBigDecimalValue(row, 11))
                    .priceChange(getDouble(row, 12))
                    .bestBid(getCellBigDecimalValue(row, 13))
                    .bestAsk(getCellBigDecimalValue(row, 14))
                    .trades(getDouble(row, 15))
                    .volume(getDouble(row, 16))
                    .turnover(getDouble(row, 17))
                    .industry(getCellStringValue(row, 18))
                    .superSector(getCellStringValue(row, 19))
                    .build();
        } catch (NumberFormatException e) {
            log.error(ERROR + "Failed to convert excel row values to requested type.");
            return null;
        }
    }

    private Segment getSegment(Row row) {
        String segment = getCellStringValue(row, 5);
        if (segment == null) {
            return UNKNOWN;
        }

        switch (segment) {
            case "Baltic Main List":
                return BALTIC_MAIN;
            case "Baltic Secondary List":
                return BALTIC_SECONDARY;
            case "First North Baltic Share List":
                return FIRST_NORTH_BALTIC;
            default:
                return UNKNOWN;
        }
    }

    private static BigDecimal getCellBigDecimalValue(Row row, int i) {
        Cell cell = row.getCell(i);
        if (cell == null) {
            return null;
        }
        return BigDecimal.valueOf(cell.getNumericCellValue());
    }

    private static String getCellStringValue(Row row, int i) {
        Cell cell = row.getCell(i);
        if (cell == null) {
            return null;
        }
        return cell.getStringCellValue();
    }

    private Double getDouble(Row row, int i) {
        Cell cell = row.getCell(i);
        if (cell == null) {
            return 0d;
        }

        return row.getCell(i).getNumericCellValue();
    }

    private void downloadCurrentDayStockData() throws IOException {
        log.info(ATTEMPT + "Starting to update current day.");
        LocalDate today = currentLocalDate();
        downloadStockFile(today);

        new Thread(() -> {
            try {
                List<StockData> downloadedStockData = insertExcelToStockData(today);
                stockService.processCurrentStockData(downloadedStockData, today);
            } catch (Exception e) {
                log.warn("Stock data migration failed", e);
                e.printStackTrace();
            }
        }).start();
    }

}