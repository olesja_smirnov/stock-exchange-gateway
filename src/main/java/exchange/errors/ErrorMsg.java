package exchange.errors;

public class ErrorMsg {

    public static final String ATTEMPT = "ATTEMPT: ";
    public static final String ERROR = "ERROR: ";
    public static final String FAILURE = "FAILURE: ";
    public static final String NOT_FOUND_STOCK = "Not found stock data";
    public static final String SUCCESS = "SUCCESS: ";

    private ErrorMsg() {

    }
}